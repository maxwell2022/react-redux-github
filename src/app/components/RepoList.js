import React, { PropTypes } from 'react';

const RepoList = ({ listing = [] }) => (
  <ul>
    {listing.map(repo => (
      <li key={repo.id}>
        <a href={repo.html_url} target="_blank">{repo.name}</a> ({repo.language} - {repo.size}kb)
      </li>
    ))}
  </ul>
);

RepoList.propTypes = {
  listing: PropTypes.array,
};

export default RepoList;
