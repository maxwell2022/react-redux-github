import 'isomorphic-fetch';
const API_ROOT = 'https://api.github.com';

export const GITHUB_API_CALL = Symbol('GITHUB_API_CALL');

const middleware = () => next => action => {
  const githubAction = action[GITHUB_API_CALL];

  // If the dispatch action is not meant to be process in this
  // middleware, we just chain it to the next one
  if (!githubAction) {
    return next(action);
  }

  const {
    types, // list of action type ([requesting, received, error])
    endpoint,
  } = githubAction;

  const [requestingType, receivedType, errorType] = types;

  const actionWith = (data) => {
    const newAction = Object.assign({}, action, data);
    delete newAction[GITHUB_API_CALL];
    return newAction;
  };

  // dispatch the action to next middleware specifying that we are currently
  // fetching data
  next(actionWith({
    type: requestingType,
    fetching: true,
  }));

  // Fetch the API
  return fetch(`${API_ROOT}${endpoint}`)
    .then(response =>
      response.json().then(json => ({ json, response }))
    )
    .then(({ response, json }) => {
      // Response is returned but not okay, we have to dispacth an error
      if (!response.ok) {
        return next(actionWith({
          type: errorType,
          error: json,
        }));
      }

      // Response is returned with correct data
      // We propagate a new action with the data payload to the next middlewares
      return next(actionWith({
        type: receivedType,
        data: json,
      }));
    })
    .catch(error => (
      // An error happened during the request, we propagate a new action to
      // notify other middlewares
      next(actionWith({
        type: errorType,
        error,
      }))
    ));
};

export default middleware;
