export default (repos = {}, action) => {
  switch (action.type) {

    case 'GITHUB_REQUEST_SENT':
      return {
        fetching: action.fetching,
        error: null,
      };

    case 'GITHUB_REQUEST_RECEIVED':
      return {
        fetching: false,
        data: action.data,
      };

    case 'GITHUB_REQUEST_ERROR':
      return {
        fetching: false,
        error: action.error,
      };

    default:
      return repos;
  }
};
