import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import repos from './repos';
import user from './user';

const rootReducer = combineReducers({
  user,
  repos,
  routing,
});

export default rootReducer;
