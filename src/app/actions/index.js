import { GITHUB_API_CALL } from 'app/middlewares/githubApi';

export const GITHUB_REQUEST_SENT = 'GITHUB_REQUEST_SENT';
export const GITHUB_REQUEST_RECEIVED = 'GITHUB_REQUEST_RECEIVED';
export const GITHUB_REQUEST_ERROR = 'GITHUB_REQUEST_ERROR';

export function githubRequestSent() {
  return {
    type: GITHUB_REQUEST_SENT,
    fetching: true,
    data: [],
    error: null,
  };
}

export function githubRequestReceived(data) {
  return {
    type: GITHUB_REQUEST_RECEIVED,
    fetching: false,
    data,
    error: null,
  };
}

export function githubError(error) {
  return {
    type: GITHUB_REQUEST_ERROR,
    fetching: false,
    data: [],
    error,
  };
}

function fetchRepo(username) {
  return {
    [GITHUB_API_CALL]: {
      types: [GITHUB_REQUEST_SENT, GITHUB_REQUEST_RECEIVED, GITHUB_REQUEST_ERROR],
      endpoint: `/users/${username}/repos`,
    },
  };
}

export function loadGithubRepository(username = 'maxwell2022') {
  return (dispatch) => {
    // Check if we need to fetch the repositories for the current user
    // looking into the existing store value
    dispatch(fetchRepo(username));
  };
}
