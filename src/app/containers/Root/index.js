import RootDev from './Root.dev';
import RootProd from './Root.prod';

export default process.env.NODE_ENV === 'production'
  ? RootProd
  : RootDev;
