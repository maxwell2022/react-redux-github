import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import RepoList from 'app/components/RepoList';
import { loadGithubRepository } from 'app/actions';
import classNames from 'classnames';

@connect(
  (state) => ({ repos: state.repos }),
  { loadGithubRepository }
)
class App extends Component {
  static propTypes = {
    loadGithubRepository: PropTypes.func.isRequired,
    repos: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.classes = classNames('container', 'flud');
  }

  componentDidMount() {
    this.props.loadGithubRepository();
  }

  render() {
    const { repos } = this.props;

    return (
      <div className={this.classes}>
        Main React appliction Container
        <RepoList listing={repos.data} />
      </div>
    );
  }
}

export default App;
