import { applyMiddleware, compose, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import githubApi from 'app/middlewares/githubApi';
import reducers from 'app/reducers';
import DevTools from 'app/containers/DevTools';

const createStoreWithMiddleware = compose(
  applyMiddleware(thunk, githubApi, logger()),
  DevTools.instrument()
)(createStore);

export default function (defaultStore = {}) {
  const store = createStoreWithMiddleware(reducers, defaultStore);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default; // eslint-disable-line global-require
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
