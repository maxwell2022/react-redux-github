import React from 'react';
import { Route } from 'react-router';
import App from 'app/containers/App';
import UserPage from 'app/containers/UserPage';
import RepoPage from 'app/containers/RepoPage';

export default (
  <Route path="/" component={App}>
    <Route path="/:username/:repoName" component={RepoPage} />
    <Route path="/:username" component={UserPage} />
  </Route>
);
